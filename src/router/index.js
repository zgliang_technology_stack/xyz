import Vue from 'vue';
import VueRouter from 'vue-router';
import LayoutPage from '@/views/layout';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: LayoutPage,
        redirect: { name: 'systemPage' },// 重定向
        children: [{
            path: '/dam',
            name: 'damPage',
            component: () => import('@/views/dam/index.vue'),
            children: [{
                path: 'system',
                name: 'systemPage',
                query: { page: 1 },
                component: () => import('@/views/dam/system/index.vue')
            }]
        }, {
            path: '/list',
            name: 'listPage',
            query: { page: 1 },
            component: () => import('@/views/list/index.vue')
        }, {
            path: '/markdown',
            name: 'MarkDown',
            component: () => import('@/views/markdown/index.vue')
        }]
    },
    // demo
    {
        path: '/demo',
        name: 'DemoPage',
        component: () => import('@/views/demo/index.vue')
    }

];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
    scrollBehavior: () => ({ y: 0 }) //切换路由后滚动到页面顶部
});

export default router;
