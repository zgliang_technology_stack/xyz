import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import '@/util/dialog.js'; // 用于拖拽弹窗 

Vue.config.productionTip = false;

// 引入 ElementUI 插件
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 弹窗全局注册
import { openDialog, DialogPage } from '@/components/Dialog/';
Vue.component('DialogPage', DialogPage);
Vue.prototype.$OpenDialog = openDialog;

// MarkDown 全局注册
import mavonEditor from 'mavon-editor';
import 'mavon-editor/dist/css/index.css';
Vue.use(mavonEditor);

// import request from '@/api/request.js';
// Vue.prototype.request = request;

import './mock/mock_all.js'; // 引入 mock 接口文件
// import axios from 'axios';
// Vue.prototype.$axios = axios;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
