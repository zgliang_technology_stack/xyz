import Vue from 'vue';
import store from '@/store';
import router from '@/router';
import DialogPage from './Dialog.vue';

export default DialogPage;

export { DialogPage };

/**
 *
 * @param {string} title 标题
 * @param {VueComponent} component dialog 内组件
 * @param {Object} dialogProps 部分 dialog props
 * @param {Object} dialogParams dialog 参数
 */
export function openDialog(title, component, dialogProps = {}, dialogParams = {}) {
    // console.log(dialogProps);

    const Instance = new Vue({
        store,
        router,
        el: document.createElement('div'),
        data() {
            const base = {
                title: title,
                destroyOnClose: false,
                visible: true,
                beforeClose: () => {
                    // console.log('beforeClose');
                    // dialog.close();
                    this.dialogProps.visible = false;
                }
            };
            // if (dialogProps.beforeClose) {
            // }
            return {
                dialogProps: Object.assign(base, dialogProps)
            };
        },
        render(h) {
            return h(
                DialogPage,
                {
                    props: this.dialogProps,
                    on: {
                        closed() {
                            // console.log('closed');
                            Instance.destroy();
                        }
                    }
                },
                [h(component, {
                    props: dialogParams, // 参数传递给子组件
                    on: {
                        closeDialog() {
                            // console.log('closeDialog');
                            Instance.destroy();
                        }
                    }
                })]
            );
        },
        // 监听,当路由发生变化的时候执行
        watch: {
            $route() {
                this.dialogProps.visible = false;
            }
        },
        methods: {
            destroy() {
                this.$destroy();
                document.body.removeChild(this.$el);
            }
        }
    });

    const com = Instance.$mount();
    document.body.appendChild(com.$el);
    const dialog = Instance.$children[0];
    dialog.open();

    return Instance;
}
