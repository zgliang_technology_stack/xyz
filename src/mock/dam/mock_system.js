import Mock from 'mockjs'; // 引入 mockjs
const basePath = '/dam/system';
/**
* @description: 渲染列表接口
* @param {
*   pageSize 每页展示条数
*   pageNum 当前页码
*   sortfiled 排序字段 id
*   sortOrder 排序  升序 asc 降序 desc
*   sysName 数据源名称
* }
*/
Mock.mock(basePath + '/list', 'post', (e) => {
    console.log('入参', JSON.parse(e.body));
    return Mock.mock({
        success: true,
        data: {
            total: 1000,
            'list|10': [
                {
                    sysId: '@cword(5,20)', // '系统ID',
                    categoryId: '@cword(5,20)', // '分类ID',
                    sysName: '@cword(5,20)', // '系统名称',
                    sysAlias: '@cword(5,20)', // '系统别名（英文）',
                    sysDescription: '@cword(5,20)', // '系统描述',
                    serverAddress: '@cword(5,20)', // '服务器地址，ip:prot',
                    accessUrl: '@cword(5,20)', // '访问地址',
                    appKey: '@cword(5,20)', // '应用key',
                    appSecret: '@cword(5,20)', // '应用密钥',
                    orderNum: '@cword(5,20)', // '排序号',
                    createDept: '@cword(5,20)', // '创建人部门',
                    createUser: '@cword(5,20)', // '创建人',
                    createTime: '@date(yyyy-mm-dd)', // '创建时间',
                    updateDept: '@cword(5,20)', // '更新人部门',
                    updateUser: '@cword(5,20)', // '更新人',
                    updateTime: '@date(yyyy-mm-dd)' // '更新时间',
                }
            ]
        }
    });
});

/**
* @description: 列表删除
* @param {
*   sysId 数据源ID (可能为数组，可能为字符串)
* }
*/
Mock.mock(basePath + '/delete', 'post', (e) => {
    console.log('入参', JSON.parse(e.body));
    return Mock.mock({
        success: true,
        message: '删除成功'
    });
});

/**
* @description: 导入-上传文件
* @param {
*   file
* }
*/
Mock.mock(basePath + '/import', 'post', (e) => {
    console.log('入参', JSON.parse(e.body));
    return Mock.mock({
        success: true,
        message: '上传成功'
    });
});

/**
* @description: 导入模板下载
* @param {}
*/
Mock.mock(basePath + '/download/template', 'post', () => {
    return Mock.mock({
        success: true,
        data: {
            file: 'https:www.baidu.com/'
        }
    });
});

/**
* @description: 导出
* @param {}
*/
Mock.mock(basePath + '/export', 'post', () => {
    return Mock.mock({
        success: true,
        data: {
            file: 'https:www.baidu.com/'
        }
    });
});