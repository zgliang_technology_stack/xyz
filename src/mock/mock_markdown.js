import Mock from 'mockjs'; // 引入 mockjs
const basePath = '/dam/datasource';
/**
* @description: markdown传值
* @param {
*   html 富文本
* }
*/
Mock.mock(basePath + '/markdown', 'post', (e) => {
    console.log(e);
    return Mock.mock({
        success: true,
        message: '上传成功'
    });
});

/**
* @description: 上传图片
* @param {
*   dsId 数据源ID (可能为数组，可能为字符串)
* }
*/
Mock.mock(basePath + '/markdown', 'get', (e) => {
    console.log(e);
    return Mock.mock({
        success: true,
        message: '上传成功'
    });
});