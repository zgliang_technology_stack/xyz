import Mock from 'mockjs'; // 引入 mockjs

Mock.mock('/api/demo', 'get', () => {
    return Mock.mock({
        'data|1-10': [
            {
                id: '@id', //随机生成id
                time: '@time', //随机生成时间
                content: '@cparagraph', //随机生成中文段落
                address: '@county(true)', //随机生成地址
                ip: '@ip', //随机生成ip
                url: '@url' //随机生成url
            }
        ]
    });
});

Mock.mock('/api/demo2', 'post', (e) => {
    console.log('入参', JSON.parse(e.body));
    return Mock.mock({
        code: '1000',
        success: true,
        message: '操作成功',
        data: {
            name: '@cname()',
            email: '@email', //随机生成邮箱
            'list|3': [
                {
                    'key|+1': 'A',
                    'value': '@sentence'
                }
            ]
        }
    });
});