import Mock from 'mockjs'; // 引入 mockjs
const basePath = '/dam/datasource';

/**
* @description: 渲染导航接口
* @param {
*   id 
*   pageSize 每页展示条数
*   pageIndex 当前页码
* }
*/
Mock.mock(basePath + '/menu', 'get', () => {
    return Mock.mock({
        success: true,
        data: [{
            id: '1',
            name: '数据资产管理平台',
            path: '/dam',
            children: [
                { name: '子系统管理', id: '1-1', path: '/dam/system' },
                { name: '数据源管理', id: '1-2', path: '/dam/datasource' },
                { name: '数据表管理', id: '1-3', path: '/dam/table' },
                { name: '表字段管理', id: '1-4', path: '/dam/tablefield' },
                { name: '文件源管理', id: '1-5', path: '/dam/fielssource' },
                { name: '文件管理', id: '1-6', path: '/dam/file' }
            ]
        }, {
            id: '2',
            name: '基础功能',
            path: '/sys',
            children: [
                {
                    name: '组织机构管理', id: '2-1', path: '/org', // 路由不跳转
                    children: [
                        { name: '部门管理', id: '2-1-1', path: '/sys/dept' },
                        { name: '用户管理', id: '2-1-2', path: '/sys/user' },
                        { name: '角色管理', id: '2-1-3', path: '/sys/role' },
                        { name: '用户角色关系管理', id: '2-1-4', path: '/sys/userrolerelation' }
                    ]
                },
                {
                    name: '菜单管理', id: '2-2', path: '/sys/menu',
                    children: [
                        { name: '菜单管理', id: '2-2-1', path: '/sys/menu' },
                        { name: '菜单授权', id: '2-2-2', path: '/sys/menupermission' },
                        { name: '操作管理', id: '2-2-3', path: '/sys/operate' },
                        { name: '操作授权', id: '2-2-4', path: '/sys/operatepermission' }
                    ]
                },
                {
                    name: '系统配置', id: '2-3', path: '/sys/config', // 路由不跳转
                    children: [
                        { name: '数据字典', id: '2-3-1', path: '/sys/dictionary' },
                        { name: '系统参数', id: '2-3-2', path: '/sys/config' }
                    ]
                }
            ]
        }, {
            id: '3',
            name: '列表组件',
            path: '/list', // 默认展示第一页数据
            children: []
        }, {
            id: '4',
            name: 'Markdown',
            path: '/markdown'
        }]
    });
});