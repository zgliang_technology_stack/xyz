import Mock from 'mockjs'; // 引入 mockjs
// const basePath = '/dam/datasource';
/**
* @description: 渲染列表接口
* @param {
*   dsName 数据源名称
*   pageSize 每页展示条数
*   pageNum 当前页码
* }
*/
// dam/datasource/list
const listPath = /^\/dam\/datasource\/list\/.*\/.*$/;
Mock.mock(listPath, 'get', (e) => {
    // const match = listPath.exec(e.url);
    console.log(e);
    return Mock.mock({
        success: true,
        data: {
            total: 1000,
            'list|10': [
                {
                    dsId: '@guid()', //  '数据源ID',
                    dsName: '@cword(0,70)', //  '数据源名称',
                    dsDescription: '@cword(0,100)', //  '数据源描述',
                    categoryId: '@cword(0,50)', //  '分类ID',
                    serverAddress: '@cword(0,60)', //  '服务器地址，ip:prot',
                    dbType: '@cword(0,10)', //  '数据库类型',
                    dbVersion: '@cword(0,10)', //  '数据库版本',
                    dbName: '@cword(0,70)', //  '数据库名称',
                    dbUser: '@cword(0,60)', //  '数据库用户名',
                    dbPassword: '@cword(0,60)', //  '数据库密码',
                    dbEncode: '@cword(0,10)', //  '数据库字符集编码',
                    connectionUrl: '@cword(0,70)', //  '连接字符串',
                    orderNum: '@integer(0,11)', //  '排序号',
                    createDept: '@cword(0,50)', //  '创建人部门',
                    createUser: '@cword(0,50)', //  '创建人',
                    createTime: '@date(yyyy-mm-dd hh:mm:ss)', //  '创建时间',
                    updateDept: '@cword(0,50)', //  '更新人部门',
                    updateUser: '@cword(0,50)', //  '更新人',
                    updateTime: '@date(yyyy-mm-dd hh:mm:ss)', //  '更新时间',
                    sysId: '@cword(0,50)' //  '所属子系统ID',
                }
            ]
        }
    });
});

/**
* @description: 列表删除
* @param {
*   dsId 数据源ID (可能为数组，可能为字符串)
* }
*/
const deletePath = /^\/dam\/datasource\/delete\/.*$/;
Mock.mock(deletePath, 'delete', (e) => {
    console.log(e);
    return Mock.mock({
        success: true,
        message: '删除成功'
    });
});