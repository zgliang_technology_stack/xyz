// 对axios进行二次封装
import axios from 'axios';
// 引入Element-ui组件方便使用提示
import ElementUI from 'element-ui';

//底下的代码是创建axios实例
const request = axios.create({
    // 配置环境
    // baseURL: process.env.VUE_APP_BASE_URL
    // 基础路径,对应vue.config文件里面的proxy代理
    baseURL: '',
    //请求不能超过5S
    timeout: 5000
});

// 请求拦截器（可以在请求发出去之前，做一些事情）
// 1. 获取用户信息
let user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
// 2. 拦截器配置
request.interceptors.request.use((config) => {
    config.headers['Content-Type'] = 'application/json;charset=utf8';
    // 在发送请求 配置请求头
    if (user) {
        config.headers.token = user.token;
    }
    return config;
}, error => {
    // 对响应错误做点什么
    return Promise.reject(error);
});

// 响应拦截器（在数据返回之后，做一些事情）
request.interceptors.response.use(
    response => {
        // 对响应数据做点什么
        let res = response.data;
        // 如果是文件
        if (response.config.responseType === 'blob') {
            return res;
        }
        // 如果是字符串,转回JSON
        if (typeof res === 'string') {
            res = res ? JSON.parse(res) : res;
        }
        // 权限验证不通过时，给出提示
        if (res.success === false) {
            ElementUI.Message({
                message: res.message,
                type: 'error'
            });
        }
        return res;
    },
    error => {
        // 对响应错误做点什么
        return Promise.reject(error);
    }
);
// 将request暴露出去便可在其余文件引入使用了
export default request;