/* eslint-disable no-undef */
import request from '@/api/request.js';

const basePath = '/dam/system';

// 获取列表数据
export function getListSystm(baseParams, extraParams) {
    var params = {
        pageSize: 10,
        pageNum: 1,
        sortfiled: 'createTime',
        sortOrder: 'desc'
    },
        commonParam = { ...params, ...baseParams };
    return request.post(`${basePath}/list`, {
        commonParam: commonParam,
        customerParam: extraParams
    });
}

// 列表删除
export function deleteList(id) {
    return request.post(`${basePath}/delete`, {
        id: id
    });
}

// 导入-文件上传
export function filesImport(params) {
    return request.post(`${basePath}/import`, {
        file: params
    });
}

// 导入模板下载
export function tempDownload() {
    return request.post(`${basePath}/download/template`);
}

// 导出
export function exportDownload() {
    return request.post(`${basePath}/export`);
}