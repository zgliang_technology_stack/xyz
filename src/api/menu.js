import request from '@/api/request.js';

const basePath = '/dam/datasource';

export function getMenu() {
    return request.get(`${basePath}/menu`);
}