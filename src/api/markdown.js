/* eslint-disable no-undef */
import request from '@/api/request.js';

const basePath = '/dam/datasource';

// 上传markdown
export function getMarkdown(params) {
    return request.post(`${basePath}/markdown`, {
        html: params.html
    });
}