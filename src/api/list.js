/* eslint-disable no-undef */
import request from '@/api/request.js';

const basePath = '/dam/datasource';

// 获取列表数据
export function getList(newParams) {
    var baseParams = {
        pageSize: 10,
        pageNum: 1
    },
        params = { ...baseParams, ...newParams };
    return request.get(`${basePath}/list/${params.pageNum}/${params.pageSize}`, { params: params });
    // return request({
    //     url: `/${basePath}/list/${params.pageNum}/${params.pageSize}`,
    //     method: 'get',
    //     params: params
    // });
}

// 列表删除
export function deleteList(id) {
    return request.delete(`${basePath}/delete/${id}`);
}