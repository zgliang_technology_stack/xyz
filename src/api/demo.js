import request from '@/api/request.js';

export function getDemo2() {
    return request.post('/api/demo2', { id: 11111 });
}

export function getDemo() {
    return request.get('/api/demo');
}